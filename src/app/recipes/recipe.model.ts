export class Recipe {
  name: string;
  description: string;
  imagePath: string;

  constructor(name: string, desc: string, imagePath: string) {
    this.name = name;
    this.description = desc;
    this.imagePath = imagePath;
  }
}

// the constructor is used to initialize the properties of the Recipe class when an instance of the class is created.
